package fr.itu.mbds.grails

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import static org.springframework.http.HttpStatus.*

@Secured(['ROLE_ADMIN','ROLE_MODO'])
class AnnonceController {

    AnnonceService annonceService
    IllustrationService illustrationService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond annonceService.list(params), model:[annonceCount: annonceService.count()]
    }

    def show(Long id) {
        def annonceDet = annonceService.get(id)
        render (view: 'show', model:[annonceDet:annonceDet]);
        def illustration = annonceDet.getIllustrations()
        respond annonceService.get(id)
    }

    def create() {
        def usersAuthors = User.all
        render (view: 'create', model:[userList:usersAuthors]);
        respond new Annonce(params)
    }

    def save(Annonce annonce) {
        if (annonce == null) {
            notFound()
            return
        }
        String imageUploadPath= grailsApplication.config.getProperty('illustrations.path')
        request.getMultiFileMap().illustration.each {

            if(it && !it.empty){
                it.transferTo(new File("${imageUploadPath}${it.originalFilename}"))
                flash.message="your.sucessful.file.upload.message"
            }
            else{
                flash.message="your.unsucessful.file.upload.message"
            }
            annonce.addToIllustrations(new Illustration(filename: "${it.originalFilename}"))
        }

        try {
            annonceService.save(annonce)
        } catch (ValidationException e) {
            respond annonce.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'annonce.label', default: 'Annonce'), annonce.id])
                redirect annonce
            }
            '*' { respond annonce, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond annonceService.get(id)
    }

    def update(Annonce annonce) {
        if (annonce == null) {
            notFound()
            return
        }

        try {
            annonceService.save(annonce)
        } catch (ValidationException e) {
            respond annonce.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'annonce.label', default: 'Annonce'), annonce.id])
                redirect annonce
            }
            '*'{ respond annonce, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        annonceService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'annonce.label', default: 'Annonce'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def uploadImage(){
        def file=request.getFile('image')
        String imageUploadPath=grailsApplication.config.imageUpload.path
        try{
            if(file && !file.empty){
                file.transferTo(new File("${imageUploadPath}/${file.name}"))
                flash.message="your.sucessful.file.upload.message"
            }
            else{
                flash.message="your.unsucessful.file.upload.message"
            }
        }
        catch(Exception e){
            log.error("Your exception message goes here",e)
        }

    }


    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'annonce.label', default: 'Annonce'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
