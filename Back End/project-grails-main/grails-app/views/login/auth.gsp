<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
        <title><g:message code='springSecurity.login.title'/></title>
        <asset:stylesheet src="login.css"/>
        <asset:stylesheet src="bootstrap.css"/>
    </head>

    <body>
        <div id="dot_1"></div>
        <div id="dot_2"></div>
        <div id="login">
            <div class="inner">
                <h2>Bonjour !</h2>
                <div class="fheader"><g:message code='springSecurity.login.header'/></div>



                <form action="${postUrl ?: '/login/authenticate'}" method="POST" id="loginForm" class="cssform" autocomplete="off">
                    <p>
                        <label for="username"><g:message code='springSecurity.login.username.label'/>:</label>
                        <input type="text" class="text_" name="${usernameParameter ?: 'username'}" id="username"/>
                    </p>

                    <p>
                        <label for="password"><g:message code='springSecurity.login.password.label'/>:</label>
                        <input type="password" class="text_" name="${passwordParameter ?: 'password'}" id="password"/>
                    </p>

                    <p id="remember_me_holder">
                        <input type="checkbox" class="chk" name="${rememberMeParameter ?: 'remember-me'}" id="remember_me" <g:if test='${hasCookie}'>checked="checked"</g:if>/>
                        <label for="remember_me"><g:message code='springSecurity.login.remember.me.label'/></label>
                    </p>

                    <g:if test='${flash.message}'>
                        <div class="login_message">${flash.message}</div>
                    </g:if>
                    <p>
                        <input type="submit" id="submit" value="${message(code: 'springSecurity.login.button')}"/>
                    </p>
                </form>
            </div>
        </div>
        <script>
            (function() {
                document.forms['loginForm'].elements['${usernameParameter ?: 'username'}'].focus();
            })();
        </script>
    </body>
</html>
