<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <asset:stylesheet src="create.css"/>
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<a href="#create-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                default="Skip to content&hellip;"/></a>

<content tag="nav">
    <ul>
        <li>
            <g:link class="list navigation-link" action="index"><i class='bx bx-detail' ></i><g:message code="default.list.label" args="[entityName]" /></g:link>
        </li>

    </ul>
</content>
<div id="create-annonce" class="content scaffold-create" role="main">
    <h2 class="titre mb-5">Création Annonce</h2>
    <g:if test="${flash.message}">
        <div class="position-fixed bottom-0 end-0 p-3 " style="z-index: 11">
            <div id="liveToast" class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-header">
                    <span  class="rounded me-2" style="padding: 1em;background: #0a53be"> </span>
                    <strong class="me-auto">LeCoinCoin</strong>
                    <small>Maintenanto</small>
                    <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
                <div class="toast-body">
                    ${flash.message}
                </div>
            </div>
        </div>
    </g:if>
    <g:hasErrors bean="${this.annonce}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.annonce}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form class="container" controller="annonce" action="save" method="POST" enctype="multipart/form-data">
        <div class="row">
            <div class="col-8 offset-2">

                 <div class="row">
                    <div class=" col mb-3">
                        <label for="title" class="form-label">Titre <span class="required-indicator">*</span></label>
                        <input type="text" name="title" class="form-control" value="" required="" id="title">
                    </div>
                 </div>
                <div class="row">
                    <div class=" col mb-3">
                        <label for="price" class="form-label">Prix <span class="required-indicator">*</span></label>
                        <input type="number decimal" name="price" class="form-control" alue="" required="" min="0.0" id="price">
                    </div>
                    <div class=" col mb-3">
                        <label for="author" class="form-label">Auteur <span class="required-indicator">*</span></label>
                        <select class="form-select" name="author.id" required="" id="author">
                            <g:each var="child" in="${userList}">
                                <option value="${child.id}">${child.username}</option>
                            </g:each>

                        </select>
                    </div>

                </div>
                <div class="row">
                    <div class=" col mb-3">
                        <label for="description" class="form-label">Description <span class="required-indicator">*</span></label>
                        <textarea class="form-control" name="description"  value="" required="" id="description" rows="3"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class=" col mb-3">
                        <label for="illustration" class="form-label">Illustrations <span class="required-indicator">*</span></label>
                        <input class="form-control" type="file"
                               id="illustration" name="illustration"
                               accept="image/png, image/jpeg" multiple/>
                    </div>
                </div>
                <div class="row">
                    <div class=" col mb-3">
                        <label for="status">Status</label>
                        <input type="hidden" name="_status"><input type="checkbox" name="status" id="status">
                    </div>
                </div>

%{--                <label for="illustration">Illustrations</label>--}%
%{--                <input class="textfield" type="file"--}%
%{--                       id="illustration" name="illustration"--}%
%{--                       accept="image/png, image/jpeg" multiple/>--}%

%{--                <div class="checkboxContainer">--}%
%{--                <label for="status">Status</label>--}%
%{--                <input type="hidden" name="_status"><input type="checkbox" name="status" id="status">--}%
%{--                </div>--}%


                <div class="row">
                    <div class=" col text-center">
                        <g:submitButton name="create" class="save buttonSubmit btn-primary mt-5"
                                        value="Créer"/>
                    </div>
                </div>
            </div>
        </div>
    </g:form>

</div>
</body>
</html>
