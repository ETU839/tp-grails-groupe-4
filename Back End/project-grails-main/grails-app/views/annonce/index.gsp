<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <asset:stylesheet src="backoffice.css"/>
</head>
<body>
<content tag="nav">
    <ul>
        <li>
            <g:link class="create navigation-link" action="create"><i class='bx bx-list-plus'></i><g:message code="default.new.label" args="[entityName]" /> </g:link>
        </li>
    </ul>
</content>
<g:if test="${flash.message}">
    <div class="position-fixed bottom-0 end-0 p-3 " style="z-index: 11">
        <div id="liveToast" class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <span  class="rounded me-2" style="padding: 1em;background: #0a53be"> </span>
                <strong class="me-auto">LeCoinCoin</strong>
                <small>Maintenanto</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                ${flash.message}
            </div>
        </div>
    </div>
</g:if>
%{--        <div class="row">--}%
%{--            <div class="col-md-3 " >--}%
%{--                <div class="card shadow rounded">--}%
%{--                    <asset:image src="undraw_Happy_announcement_re_tsm0.png" class="card-img-top"/>--}%
%{--                    <div class="card-body">--}%
%{--                        <h4 class="card-title">CRUD Annonce</h4>--}%
%{--                        <p class="card-text">This is some text within a card body.</p>--}%
%{--                    </div>--}%
%{--                </div>--}%
%{--            </div>--}%
%{--            <div class="col-md-9" >--}%
%{--                <div class="card">--}%
%{--                    <div class="card-body">--}%
%{--                        <h5 class="card-title">Liste des annonces présents dans la base</h5>--}%
%{--                        <div id="list-annonce" class="content scaffold-list mt-5" role="main">--}%
%{--                            <g:if test="${flash.message}">--}%
%{--                                <div class="message" role="status">${flash.message}</div>--}%
%{--                            </g:if>--}%
%{--                            <f:table class="table table-bordered" collection="${annonceList}"  />--}%

%{--                            <div class="pagination">--}%
%{--                                <g:paginate max='5' total="${annonceCount ?: 0}"  />--}%
%{--                            </div>--}%
%{--                        </div>--}%
%{--                    </div>--}%
%{--                </div>--}%
%{--            </div>--}%
%{--        </div>--}%

<div class="row mb-2">
    <div class=" entete">
        %{--            <asset:image src="undraw_Happy_announcement_re_tsm0.png" class="card-img-top"/>--}%
        <div class="card-body">
            <h4 class="card-title">Liste des annonces </h4>
            <p class="card-text">Le nombre total d'annonces actuel est ${annonceCount}</p>
        </div>
    </div>
</div>

<g:each var="a" in="${annonceList}">
    <div class="row mt-4 ">
        <div class="card cardAnnonce shadow">
            <div class="card-body">
                <div class="imageCardPart">
                    <div>
                        <g:each var="child" in="${a.getIllustrations()}">
                            <g:if test="${child != null}">
                                <li class="mb-1">
                                    <asset:image src="${child.filename}" class="img-thumbnail"/>
                                </li>
                            </g:if>
                        </g:each>
                    </div>
                    <g:if test="${a.getIllustrations()[0] != null}">
                        <div class="firstImage mx-2">
                            <asset:image src="${a.getIllustrations()[0].filename}" class="img-thumbnail"/>
                        </div>
                    </g:if>
                </div>
                <div class="titreCardPart">
                    <p class="card-title">${a.title} </p>
                    <div class="prixCardPart">
                        <g:formatNumber number="${a.price}" type="number" maxFractionDigits="0" /><span>,00€</span>
                    </div>
                    <p class="descCardPart"> ${a.description}</p>
                </div>


                <div class="actionCardPart">
                    <a href="show/${a.id}"><button type="button" class="btn btn-outline-secondary">Voir le produit</button></a>
                    <a href="edit/${a.id}"><button type="button" class="btn btn-outline-warning">Modifier le produit</button></a>
                    <g:form resource="${a}" method="DELETE">
                        <button class="delete btn btn-outline-danger" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Voulez-vous vraiment supprimer ce produit ?')}');" > Supprimer le produit</button>
                    </g:form>
                </div>

            </div>
        </div>
    </div>
</g:each>

</body>
