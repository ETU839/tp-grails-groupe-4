<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
    <title><g:message code="default.edit.label" args="[entityName]" /></title>
%{--    <asset:stylesheet src="annonce.css"/>--}%
</head>
<body>
<a href="#edit-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<content tag="nav">
    <ul>
        <li>
            <g:link class="create navigation-link" action="create"><i class='bx bx-list-plus'></i><g:message code="default.new.label" args="[entityName]" /> </g:link>
        </li>
        <li>
            <g:link class="list navigation-link" action="index"><i class='bx bx-detail' ></i><g:message code="default.list.label" args="[entityName]" /></g:link>
        </li>

    </ul>
</content>
<div id="edit-annonce" class="content scaffold-edit" role="main">
    <h4 style="text-align: center; font-size: 2em" class="mt-3 mb-4"><g:message code="default.edit.labe" default="Mis à jour annonce" args="[entityName]" /></h4>
    <g:if test="${flash.message}">
        <div class="position-fixed bottom-0 end-0 p-3 " style="z-index: 11">
            <div id="liveToast" class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-header">
                    <span  class="rounded me-2" style="padding: 1em;background: #0a53be"> </span>
                    <strong class="me-auto">LeCoinCoin</strong>
                    <small>Maintenanto</small>
                    <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
                <div class="toast-body">
                    ${flash.message}
                </div>
            </div>
        </div>
    </g:if>
    <g:hasErrors bean="${this.annonce}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.annonce}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.annonce}" method="PUT">
        <g:hiddenField name="version" value="${this.annonce?.version}" />
        <fieldset class="form">

%{--            <g:each in="annonce">${it.illustrations}</g:each>--}%
            <div class="row mb-3">
                <div class="col">
                    <label for="title" class="form-label">Title</label>
                    <g:textField class="form-control" name="title" value="${this.annonce.title}" id="title"/>
                </div>
                <div class="col">
                    <label for="price" class="form-label">Price</label>
                    <g:textField  type="number decimal" class="form-control" name="price" value="${this.annonce.price}" id="price"/>
                </div>
            <div class="col"> <f:field  bean="annonce" property="author"  /></div>
                <div class="col">
                    <label for="status">Status</label><input type="hidden" name="_status">
                    <input type="checkbox" name="status" checked="checked" id="status">
                </div>
            </div>
            <div class="row">
                <div class="mb-3">
                    <label for="description" class="form-label">Description</label>
                    <g:textArea name="description"  class="form-control"  id="description" value="${this.annonce.description}"/>
                </div>
            </div>

        </fieldset>
        <fieldset class="text-center">
            <input class="save btn btn-warning " type="submit" value="${message(code: 'default.button.update.label', default: 'Mettre à jour')}" />
        </fieldset>
    </g:form>
%{--    <div class="row">--}%
%{--        <span id="illustrations-label" class="property-label">Illustrations</span>--}%
%{--        <div class="property-value" aria-labelledby="illustrations-label">--}%
%{--            <div style="list-style: none;display: flex;--}%
%{--            justify-content: space-between;--}%
%{--            align-items: center;">--}%
%{--                <g:each var="child" in="${this.annonce.illustrations}">--}%
%{--                    <g:if test="${child != null}">--}%
%{--                        <div style="position: relative" class="mt-3">--}%
%{--                            <asset:image src="${child.filename}" class="shadow-sm imageAsinaHover" style="width: 10em;height: 15em;object-fit: cover;border-radius: 15px"/>--}%
%{--                        </div>--}%
%{--                    </g:if>--}%
%{--                </g:each>--}%
%{--            </div>--}%


%{--        </div>--}%
%{--    </div>--}%
</body>
</html>
