<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
        <asset:stylesheet src="annonce.css"/>
    </head>
    <body>
        <a href="#show-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <content tag="nav">
            <ul>
                <li>
                    <g:link class="create navigation-link" action="create"><i class='bx bx-list-plus'></i><g:message code="default.new.label" args="[entityName]" /> </g:link>
                </li>
                <li>
                    <g:link class="list navigation-link" action="index"><i class='bx bx-detail' ></i><g:message code="default.list.label" args="[entityName]" /></g:link>
                </li>

            </ul>
        </content>
        <div id="show-annonce" class="content scaffold-show" role="main">
%{--            <h1><g:message code="default.show.label" args="[entityName]" /></h1>--}%
            <g:if test="${flash.message}">
                <div class="position-fixed bottom-0 end-0 p-3 " style="z-index: 11">
                    <div id="liveToast" class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
                        <div class="toast-header">
                            <span  class="rounded me-2" style="padding: 1em;background: #0a53be"> </span>
                            <strong class="me-auto">LeCoinCoin</strong>
                            <small>Maintenanto</small>
                            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                        </div>
                        <div class="toast-body">
                            ${flash.message}
                        </div>
                    </div>
                </div>
            </g:if>

            <div class="row">
                <div class="col-8">
                    <div class="row">
                    <g:each var="child" in="${annonceDet.getIllustrations()}">
                        <g:if test="${child != null}">
                            <div  class="col containerBoutonDelete">
                                <g:form resource="${child}" controller="illustration" method="DELETE">
                                    <asset:image src="${child.filename}"  class="img-fluid shadow-sm" style="min-height: 25em;max-height: 25em;object-fit: cover;border-radius: 13px"/>
                                %{--                                            <fieldset class="buttons">--}%
                                    <input
                                            class="delete boutonDelete"
                                            type="submit"
                                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"
                                    />
                                %{--                                            </fieldset>--}%
                                </g:form>

                            </div>
                        </g:if>
                    </g:each>
                    </div>
                    <div class="row mt-5">
                            <h6 style="color: #1e2125;font-size: 2em;font-weight: 600">${annonceDet.title}</h6>
                    </div>
%{--                    <div class="row mt-3">--}%
%{--                        <div class="prixCardPart" style="font-size: 25px">--}%
%{--                            <g:formatNumber number="${annonceDet.price}" type="number" maxFractionDigits="0" /><span>,00€</span>--}%
%{--                        </div>--}%
%{--                    </div>--}%
                    <div class="row mt-3">
%{--                        <p class="card-text mb-2">${annonceDet.description}</p>--}%
%{--                        <p style="color: #5c636a;font-size: 13px">${annonceDet.dateCreated.toLocaleString()}</p>--}%
                    </div>
                </div>
                <div class="col-4">
                    <div class="card" style="border: none">
                        <div class="card-body">
                            <h5 class="card-title mb-3 text-dark">Description</h5>
                            <p class="card-text mb-1">${annonceDet.description}</p>
                            <p style="color: #5c636a;font-size: 11px" class="mb-4">
                                Publié le ${annonceDet.dateCreated.toLocaleString()}
                                par
                                <a href="/user/show/${annonceDet.author.getId()}"> <span class="card-text text-capitalize text-primary">${annonceDet.author.username}</span></a>
                            </p>

                            <h5 class="card-title mb-3 text-dark">Prix</h5>
                            <div class="prixCardPart mb-4" style="font-size: 25px">
                                <g:formatNumber number="${annonceDet.price}" type="number" maxFractionDigits="0" /><span>,00€</span>
                            </div>

                            <g:form resource="${this.annonce}" method="DELETE" style="width: 100%" >
                                <g:link class="edit btn btn-outline-warning" style="width: 49%" action="edit" resource="${this.annonce}">Modifier</g:link>
                                <button class="delete btn btn-outline-danger"  type="submit" style="width: 49%" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">Supprimer</button>
                            %{--                                <input class="delete"  type="submit" value="${message(code: 'Supprimer', default: 'Supprimer')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />--}%
                            </g:form>
                            <g:form controller="annonce" action="saveImage" method="POST" enctype="multipart/form-data">
                                <div class="mt-4">
                                    <h5 class="card-title mb-3 text-dark">Ajouter des illustrations</h5>
                                    <div class="col mt-3">
                                        <input type="hidden" id="idA" name="idA" value="${annonceDet.getId()}">
    %{--                                    <div class="fieldcontain">--}%
                                            <input type="file"
                                                   class="form-control"
                                                   id="illustration" name="illustration"
                                                   accept="image/png, image/jpeg" multiple />
    %{--                                    </div>--}%
                                    </div>
                                    <div class="col mt-3 ">
                                        <g:submitButton name="create" class="save btn btn-primary" style="width: 100%" value="${message(code: 'default.button.create.labael', default: 'Ajouter')}" />
                                    </div>
                                </div>
                            </g:form>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </body>
</html>
