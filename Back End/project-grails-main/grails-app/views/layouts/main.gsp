<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
    <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />

    <asset:stylesheet src="application.css"/>

    <asset:stylesheet src="layout.css"/>
    <link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
    <g:layoutHead/>
</head>
<body>
%{--DEFAULT--}%
%{--    <div class="navbar navbar-default navbar-static-top" role="navigation">--}%
%{--        <div class="container">--}%
%{--            <div class="navbar-header">--}%
%{--                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">--}%
%{--                    <span class="sr-only">Toggle navigation</span>--}%
%{--                    <span class="icon-bar"></span>--}%
%{--                    <span class="icon-bar"></span>--}%
%{--                    <span class="icon-bar"></span>--}%
%{--                </button>--}%
%{--                <a class="navbar-brand" href="/#">--}%
%{--		            <asset:image src="grails.svg" alt="Grails Logo"/>--}%
%{--                </a>--}%
%{--            </div>--}%
%{--            <div class="navbar-collapse collapse" aria-expanded="false" style="height: 0.8px;">--}%
%{--                <ul class="nav navbar-nav navbar-right">--}%
%{--                    <g:pageProperty name="page.nav" />--}%
%{--                </ul>--}%
%{--            </div>--}%
%{--        </div>--}%
%{--    </div>--}%
%{--DEFAULT--}%
%{--SIDEBAR--}%
%{--<header class="header" id="header">--}%
%{--    <div class="header_toggle"> <i class='bx bx-menu' id="header-toggle"></i> </div>--}%
%{--    <div class="header_img"> <img src="https://i.imgur.com/hczKIze.jpg" alt=""> </div>--}%
%{--</header>--}%
%{--<div class="l-navbar" id="nav-bar">--}%
%{--    <nav class="navb">--}%
%{--        <div> <a href="#" class="nav_logo"> <i class='bx bx-layer nav_logo-icon'></i> <span class="nav_logo-name">BBBootstrap</span> </a>--}%
%{--            <div class="nav_list">--}%
%{--                <a href="#" class="nav_link active">--}%
%{--                    <i class='bx bx-grid-alt nav_icon'></i>--}%
%{--                    <span class="nav_name">Dashboard</span>--}%
%{--                </a>--}%
%{--                <a href="/annonce/index" class="nav_link">--}%
%{--                    <i class='bx bx-message-square-detail nav_icon'></i>--}%
%{--                    <span class="nav_name">Annonce</span>--}%
%{--                </a>--}%
%{--                <a href="/user/index" class="nav_link">--}%
%{--                    <i class='bx bx-user nav_icon'></i>--}%
%{--                    <span class="nav_name">Utilisateur</span>--}%
%{--                </a>--}%

%{--            </div>--}%
%{--        </div>--}%
%{--        <a href="#" class="nav_link">--}%
%{--            <i class='bx bx-log-out nav_icon'></i>--}%
%{--            <span class="nav_name">Déconnexion</span>--}%
%{--        </a>--}%
%{--    </nav>--}%
%{--</div>--}%
%{--SIDEBAR--}%

<!--Container Main start-->
%{--<div class="height-100 mainContainer">--}%
%{--    <g:layoutBody/>--}%
%{--</div>--}%
<div class="row containerBe">
    <div class="sidebarPart navbar-dark bg-dark">
        <div class="logoPart row">
            <div class="col-2" id="logoImage">
                <i class='bx bx-layer nav_logo-icon'></i>
            </div>
            <div class="col-8" id="logoText">
                LeCoinCoin
                <span>Backoffice</span>
            </div>
        </div>
        <div class="row navigation mt-5">
            <ul>
%{--                <li>--}%
%{--                    <a class="navigation-link" href="/">--}%
%{--                        <i class='bx bx-home'></i></i>  Accueil--}%
%{--                    </a>--}%
%{--                </li>--}%
                <li>
                    <a class="navigation-link" href="/annonce/index">
                        <i class='bx bx-news' ></i> Annonces
                    </a>
                </li>
                <li>
                    <a class="navigation-link" href="/user/index">
                        <i class='bx bx-group'></i> Utilisateurs
                    </a>
                </li>
            </ul>
            <hr class="my-4 hrclass text-white">
            <ul >
                <g:pageProperty name="page.nav" />
            </ul>
        </div>
        <div class="row profilePart dropup">
%{--            <div class="col-2" id="avatarImage">--}%
%{--                <asset:image src="avatar.png"/>--}%
%{--            </div>--}%
            <div class="col-12 text-white" id="avatarText">
                <sec:username/>
            </div>
            <a href="/logout/index" class="text-white"><i class='bx bx-log-out-circle'></i></a>
        </div>
    </div>
    <div class="containerPart">
        <g:layoutBody/>
    </div>
</div>

%{--<div class="footer" role="contentinfo"></div>--}%


<div id="spinner" class="spinner" style="display:none;">
    <g:message code="spinner.alt" default="Loading&hellip;"/>
</div>

<asset:javascript src="application.js"/>

</body>
</html>
