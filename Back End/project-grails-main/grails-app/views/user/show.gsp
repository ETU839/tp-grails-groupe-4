<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <content tag="nav">
            <ul>
                <li>
                    <g:link class="create navigation-link" action="create"><i class='bx bx-list-plus'></i><g:message code="default.new.label" args="[entityName]" /> </g:link>
                </li>
                <li>
                    <g:link class="list navigation-link" action="index"><i class='bx bx-detail' ></i><g:message code="default.list.label" args="[entityName]" /></g:link>
                </li>

            </ul>
        </content>
        <div id="show-user container" class="content scaffold-show" role="main">
        <div class="row col-8 offset-2 text-center">
            <h2 class="mb-4">Détails de l'utilisateur</h2>
            <g:if test="${flash.message}">
                <div class="position-fixed bottom-0 end-0 p-3 " style="z-index: 11">
                    <div id="liveToast" class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
                        <div class="toast-header">
                            <span  class="rounded me-2" style="padding: 1em;background: #0a53be"> </span>
                            <strong class="me-auto">LeCoinCoin</strong>
                            <small>Maintenanto</small>
                            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                        </div>
                        <div class="toast-body">
                            ${flash.message}
                        </div>
                    </div>
                </div>
            </g:if>
            <f:display bean="user" except="password" />
            <g:form resource="${this.user}" method="DELETE" class="mt-4">
%{--                <fieldset class="buttons">--}%
                    <g:link class="edit btn btn-warning" action="edit" resource="${this.user}"><g:message code="default.button.edi.label" default="Modifier" /></g:link>
                    <input class="delete btn btn-danger" type="submit" value="${message(code: 'default.button.delete.labl', default: 'Supprimer')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
%{--                </fieldset>--}%
            </g:form>
        </div>
        </div>
    </body>
</html>
