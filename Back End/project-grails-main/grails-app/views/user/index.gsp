<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <asset:stylesheet src="backoffice.css"/>
</head>

<body>
<a href="#list-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                           default="Skip to content&hellip;"/></a>
<content tag="nav">
    <ul>
        <li>
            <g:link class="create navigation-link" action="create"><i class='bx bx-list-plus'></i><g:message code="default.new.label" args="[entityName]" /> </g:link>
        </li>
    </ul>
</content>

<div class="row mb-2">
    <div class=" entete">
        %{--            <asset:image src="undraw_Happy_announcement_re_tsm0.png" class="card-img-top"/>--}%
        <div class="card-body">
            <h4 class="card-title">Liste des utilisateurs </h4>
            <p class="card-text">Le nombre total d'utilisateurs actuel est ${userCount}</p>
        </div>
    </div>
</div>

<g:if test="${flash.message}">
    <div class="position-fixed bottom-0 end-0 p-3 " style="z-index: 11">
        <div id="liveToast" class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <span  class="rounded me-2" style="padding: 1em;background: #0a53be"> </span>
                <strong class="me-auto">LeCoinCoin</strong>
                <small>Maintenanto</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">
                ${flash.message}
            </div>
        </div>
    </div>
</g:if>
%{--            <f:table collection="${userList}" />--}%
<div class="row mt-4">
    <div class="row bg-white shadow" style="border-radius: 10px">
    <table class="table table-borderless">
        <tr>
            <th>Nom</th>
            <th>Compte bloqué</th>
            <th>Compte expiré</th>
            <th>Compte activé</th>
            <th>Annonces publiés</th>
            <th></th>
        </tr>
        <g:each in="${userList}" var="user" status="i">

            <tr>
                <td style="text-transform: capitalize"><a href="/user/show/${user.id}">${user.username}</a></td>
                <td style="font-size: 1em !important;">
                    <g:if test="${user.accountLocked}">
                        <span class="badge rounded-pill bg-danger">Oui</span>
                    </g:if>
                    <g:else>
                        <span class="badge rounded-pill bg-success">Non</span>
                    </g:else>
                </td>
                <td style="font-size: 1em !important;">
                    <g:if test="${user.accountExpired}">
                        <span class="badge rounded-pill bg-danger">Oui</span>
                    </g:if>
                    <g:else>
                        <span class="badge rounded-pill bg-success">Non</span>
                    </g:else>
                </td>
                <td>
%{--                    ${user.enabled ? "Oui" : "Non"}--}%
                    <g:if test="${user.accountExpired}">
                        <span class="badge rounded-pill bg-danger"> </span> Inactif
                    </g:if>
                    <g:else>
                        <span class="badge rounded-pill bg-success"> </span> Actif
                    </g:else>
                </td>
                <td>${user.annonces.size()}</td>
                <td style="font-size: 1.5em;" ><g:link controller="user" action="edit" id="${user.id}"><i class='bx bx-edit-alt' ></i></g:link></td>
            </tr>

        </g:each>

    </table>
    </div>
</div>
</body>
</html>