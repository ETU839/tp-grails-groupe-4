<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <asset:stylesheet src="create.css"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<a href="#create-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                             default="Skip to content&hellip;"/></a>

<content tag="nav">
    <ul>
        <li>
            <g:link class="list navigation-link" action="index"><i class='bx bx-detail' ></i><g:message code="default.list.label" args="[entityName]" /></g:link>
        </li>

    </ul>
</content>

<div id="create-user" class="content scaffold-create" role="main">
    <h2 class="titre">Création Utilisateur</h2>

    <g:if test="${flash.message}">
        <div class="position-fixed bottom-0 end-0 p-3 " style="z-index: 11">
            <div id="liveToast" class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-header">
                    <span  class="rounded me-2" style="padding: 1em;background: #0a53be"> </span>
                    <strong class="me-auto">LeCoinCoin</strong>
                    <small>Maintenanto</small>
                    <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
                <div class="toast-body">
                    ${flash.message}
                </div>
            </div>
        </div>
    </g:if>
    <g:hasErrors bean="${this.user}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.user}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form class="formulaireContainer" controller="user" action="create" method="POST" enctype="multipart/form-data">
    %{--                    <f:all bean="user"/>--}%

        <label for="username">Nom
            <span class="required-indicator">*</span>
        </label><input class="textfield" type="text" name="username" value="" required="" id="username">

        <label for="password">Mot de passe
            <span class="required-indicator">*</span>
        </label><input class="textfield" type="password" name="password" required="" value="" id="password">


        <div class="checkboxContainer">
            <label for="passwordExpired">Mot de passe expiré</label><input type="hidden"
                                                                           name="_passwordExpired"><input
                type="checkbox" name="passwordExpired" id="passwordExpired">
        </div>


        <div class="checkboxContainer">
                <label for="accountLocked">Compte bloqué</label><input type="hidden" name="_accountLocked"><input
                    type="checkbox" name="accountLocked" id="accountLocked">
        </div>
        <div class="checkboxContainer">

                <label for="accountExpired">Compte expiré</label><input type="hidden" name="_accountExpired"><input
                    type="checkbox" name="accountExpired" id="accountExpired">
        </div>
        <div class="checkboxContainer">

                <label for="enabled">Compte activé</label><input type="hidden" name="_enabled"><input
                    type="checkbox"
                    name="enabled"
                    checked="checked"
                    id="enabled">
        </div>


    %{--                    <div class="fieldcontain">--}%
    %{--                        <label for="annonces">Annonces</label><ul></ul><a href="/annonce/create?user.id=">Add Annonce</a>--}%
    %{--                    </div>--}%

        <label for="role">Rôle</label>
        <select class="textfield" name="role">
            <option value="ROLE_ADMIN">Admin</option>
            <option value="ROLE_MODO">Modérateur</option>
            <option value="ROLE_CLIENT">Client</option>
        </select>

        <g:submitButton name="create" class="save buttonSubmit btn-primary mt-5"
                        value="Créer"/>
    </g:form>
</div>
</body>
</html>
