<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <asset:stylesheet src="create.css"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<a href="#edit-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                           default="Skip to content&hellip;"/></a>

%{--<div class="nav" role="navigation">--}%
%{--    <ul>--}%
%{--        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>--}%
%{--        <li><g:link class="create" action="create"><g:message code="default.new.label"--}%
%{--                                                              args="[entityName]"/></g:link></li>--}%
%{--    </ul>--}%
%{--</div>--}%
<content tag="nav">
    <ul>
        <li>
            <g:link class="create navigation-link" action="create"><i class='bx bx-list-plus'></i><g:message code="default.new.label" args="[entityName]" /> </g:link>
        </li>
        <li>
            <g:link class="list navigation-link" action="index"><i class='bx bx-detail' ></i><g:message code="default.list.label" args="[entityName]" /></g:link>
        </li>

    </ul>
</content>

<div id="edit-user" class="content scaffold-edit" role="main">
    <h2 class="titre">Mise à jour</h2>
    <g:if test="${flash.message}">
        <div class="position-fixed bottom-0 end-0 p-3 " style="z-index: 11">
            <div id="liveToast" class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-header">
                    <span  class="rounded me-2" style="padding: 1em;background: #0a53be"> </span>
                    <strong class="me-auto">LeCoinCoin</strong>
                    <small>Maintenanto</small>
                    <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
                <div class="toast-body">
                    ${flash.message}
                </div>
            </div>
        </div>
    </g:if>
    <g:hasErrors bean="${this.user}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.user}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form class="formulaireContainer" resource="${this.user}" method="PUT">
        <g:hiddenField name="version" value="${this.user?.version}"/>
        <g:hiddenField name="id" value="${this.user.id}"/>
        <label>Nom :</label>
        <g:textField class="textfield" name="username" value="${this.user.username}"/>
%{--        <label>Mot de passe :</label>--}%
%{--        <g:textField class="textfield" name="password" value="${this.user.password}"/>--}%
        <div class="checkboxContainer">
            <label>Mot de passe expiré :</label>
            <input type="checkbox" name="passwordExpired" ${this.user.passwordExpired ? "checked" : ""}/>
        </div>

        <div class="checkboxContainer">
            <label>Compte bloqué :</label>
            <input type="checkbox" name="accountLocked" ${this.user.accountLocked ? "checked" : ""}/>
        </div>

        <div class="checkboxContainer">
            <label>Compte expiré :</label>
            <input type="checkbox" name="accountExpired" ${this.user.accountExpired ? "checked" : ""}/>
        </div>

        <div class="checkboxContainer">
            <label>Compte activé :</label>
            <input type="checkbox" name="enabled" ${this.user.enabled ? "checked" : ""}/>
        </div>
%{--        <label>Annonces :</label>--}%
%{--        <g:each in="${this.user.annonces}" var="annonce">--}%
%{--            <il>--}%
%{--                <g:link class="annonceLink" controller="annonce" action="show" id="${annonce.id}">${annonce.title}</g:link>--}%
%{--            </il>--}%
%{--        </g:each>--}%
        <label>Rôle :</label>
        <select class="textfield" name="role">
            <g:if test="${this.user.getAuthorities().authority[0] == 'ROLE_ADMIN'}">
                <option value="ROLE_ADMIN" selected>Admin</option>
            </g:if>
            <g:else>
                <option value="ROLE_ADMIN">Admin</option>
            </g:else>
            <g:if test="${this.user.getAuthorities().authority[0] == 'ROLE_MODO'}">
                <option value="ROLE_MODO" selected>Modérateur</option>
            </g:if>
            <g:else>
                <option value="ROLE_MODO">Modérateur</option>
            </g:else>
            <g:if test="${this.user.getAuthorities().authority[0] == 'ROLE_CLIENT'}">
                <option value="ROLE_CLIENT" selected>Client</option>
            </g:if>
            <g:else>
                <option value="ROLE_CLIENT">Client</option>
            </g:else>
        </select>

        <input type="submit" class="save buttonSubmit"
               value="Valider"/>
    </g:form>
</div>
</body>
</html>
