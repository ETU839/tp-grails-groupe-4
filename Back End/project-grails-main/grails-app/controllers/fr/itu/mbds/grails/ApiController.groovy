package fr.itu.mbds.grails

import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured

import javax.servlet.http.HttpServletResponse

@Secured(['ROLE_ADMIN','ROLE_MODO'])
class ApiController {
    AnnonceService annonceService
    UserService userService

//    Singleton : GET / PUT / PATCH / DELETE
    def user() {
        if (!params.id)
            return response.status = 400
        switch (request.getMethod()) {
            case "GET":
                def userInstance = User.get(params.id)
                if (!userInstance)
                    return response.status = 404
                serializeData(userInstance, request.getHeader("Accept"))
                break
            case "PUT":
                def userInstance = User.get(params.id)
                if (!userInstance)
                    return response.status = 404

                def data = request.getJSON()
                data.each {
                    def ligne = it.toString()
                    def raw = ligne.split('=')
                    userInstance.setProperty(raw[0],data.getAt(raw[0]))
                }

                userService.save(userInstance)
                render "modification effectuée "
                break
            case "PATCH":
                break
            case "DELETE":
                userService.delete(params.id)
                render " utilisateur supprimé"
                break
            default:
                return response.status = 405
                break
        }
        return response.status = 406
    }

//    Collection : GET / POST
    def users() {
        println(request.getMethod())
        switch (request.getMethod()) {
            case "GET":
                def userInstance = User.all
                if (!userInstance)
                    return response.status = 404
                serializeData(userInstance, request.getHeader("Accept"))
                break
            case "POST":
                def data = request.getJSON()
                def userNew = new User(username: data.getAt("username"), password: data.getAt("password")).save()
                serializeData(userNew, request.getHeader("Accept"))
                break
            default:
                return response.status = 405
                break
        }
    }

//    Singleton : GET / PUT / PATCH / DELETE
    def annonce() {
        if (!params.id)
            return response.status = 400
        switch (request.getMethod()) {
            case "GET":
                def annonceInstance = Annonce.get(params.id)
                if (!annonceInstance)
                    return response.status = 404
                serializeData(annonceInstance, request.getHeader("Accept"))
                break
            case "PUT":
                def annonceInstance = Annonce.get(params.id)
                if (!annonceInstance)
                    return response.status = 404

                def data = request.getJSON()
                data.each {
                    def ligne = it.toString()
                    def raw = ligne.split('=')
                    annonceInstance.setProperty(raw[0],data.getAt(raw[0]))
                }

                annonceService.save(annonceInstance)
                render "modification effectuée "
                break
            case "PATCH":
                break
            case "DELETE":
                annonceService.delete(params.id)
                render " annonce supprimée"
                break
            default:
                return response.status = 405
                break
        }
        return response.status = 406
    }

//    Collection : GET / POST
    def annonces() {
        switch (request.getMethod()) {
            case "GET":
                def annonceInstance = Annonce.all
                if (!annonceInstance)
                    return response.status = 404
                serializeData(annonceInstance, request.getHeader("Accept"))
                break
            case "POST":
                def data = request.getJSON()
                def annonceNew = new Annonce( title: data["title"],
                        description: data["description"],
                        price: data["price"],
                        status: data["status"],
                        dateCreated: data["dateCreated"],
                        lastUpdated: data["lastUpdated"],
                        author: data["author"]
                )
                annonceService.save(annonceNew)
                if(annonceNew)
                    serializeData(annonceNew, request.getHeader("Accept"))
                else{
                    render "erreur de l'annonce"
                }

                break
            default:
                return response.status = 405
                break
        }
    }


    def findAnnonce() {
        switch (request.getMethod()) {
            case "POST":
                println("ito eee")
                def toSearch = request.getJSON().getAt("toSearch")
                def listeAnnonce = []

                Annonce.all.each {
                    if(it.title.toUpperCase().contains(toSearch.toUpperCase()) || it.description.toUpperCase().contains(toSearch.toUpperCase())){
                        listeAnnonce.add(it)
                    }
                }

                serializeData(listeAnnonce, request.getHeader("Accept"))
                break
            default:
                return response.status = 405
                break
        }
    }


    def renderThis(Object instance, String accept)
    {
        switch(accept)
        {
            case "xml":
            case "text/xml":
            case "application/xml":
                render instance as XML
                break;
            case "json":
            case "text/json":
            case "application/json":
                render instance as JSON
                break;
        }
    }

    def serializeData(object, format)
    {
        switch (format)
        {
            case 'json':
            case 'application/json':
            case 'text/json':
                render object as JSON
                break
            case 'xml':
            case 'application/xml':
            case 'text/xml':
                render object as XML
                break
            default:
                render object as XML
                break
        }
    }
}
