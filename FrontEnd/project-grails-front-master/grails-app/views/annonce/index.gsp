<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title> Annonces</title>
    <asset:stylesheet src="frontoffice.css"/>

</head>
<body>
<content tag="navann">
        <li>
            <g:link class="create dropdown-item" action="index"><g:message code="default.list.lel" default="Liste" args="[entityName]" /> </g:link>
        </li>
</content>


<div class="row mt-5">
    <% def i  = 1 %>

    <g:each var="a" in="${annonceDet}">

            <div class=" col-3 mt-4">
                <div class="shadow"  style="border-radius: 10px">
                    <div class="">
                        <div class="imageCardPart">
                            <g:if test="${ a.illustrations != null && a.getIllustrations()[0] != null}">
                                <div class="firstImage text-center">
                                    <asset:image src="close-up-photography-of-leaves-with-droplets-807598.jpg" class="img-fluid" style="border-radius: 10px"/>
                                </div>
                            </g:if>
                        </div>
                        <div class="titreCardPart mx-3 my-3 " >
                            <h2 class="mb-1" style="font-size: 1.2vw;font-weight: 600" > ${a.title} </h2>
                            <p class="descCardPart" style="font-size: 0.7vw;color: #7d7d7d"> ${a.description}</p>
                            <div class="prixCardPart mt-3" style="font-size: 1.5vw;line-height: 30px;font-weight: 700">
                                <g:formatNumber number="${a.price}" type="number" maxFractionDigits="0" /><span style="font-weight: 500;line-height: 10px;font-size: 0.8vw">,00€</span>
                            </div>
                        </div>


                        <div class="actionCardPart text-center">
                            <a href="show/${a.id}">
                                <button class="learn-more">
                                    <span class="circle" aria-hidden="true">
                                        <span class="icon arrow">  </span>
                                    </span>
                                    <span class="button-text"> Voir plus </span>
                                </button>
                            </a>
                        </div>

                    </div>
                </div>
            </div>

    </g:each>
</div>


</body>
</html>