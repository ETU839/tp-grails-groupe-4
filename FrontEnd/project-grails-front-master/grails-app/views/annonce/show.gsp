<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<a href="#show-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<content tag="navann">
    <li>
        <g:link class="create dropdown-item" action="index"><g:message code="default.list.bel" default="Liste" args="[entityName]" /> </g:link>
    </li>
</content>
<div id="show-annonce" class="content scaffold-show" role="main">
%{--            <h1><g:message code="default.show.label" args="[entityName]" /></h1>--}%
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <div class="row mt-5">
        <div class="col-8">
            <div class="row">
                <g:each var="child" in="${annonceDet.getIllustrations()}">
                    <g:if test="${child != null}">
                        <div class="col">
                            <asset:image src="close-up-photography-of-leaves-with-droplets-807598.jpg" class="img-fluid shadow-sm" style="min-height: 25em;max-height: 25em;object-fit: cover;border-radius: 13px"/>
                        </div>
                    </g:if>
                </g:each>
            </div>
            <div class="row mt-5">
                <h6 style="color: #1e2125;font-size: 2em;font-weight: 600">${annonceDet.title}</h6>
            </div>
            <div class="row mt-3">
            </div>
        </div>
        <div class="col-4">
            <div class="card" style="border: none">
                <div class="card-body">
                    <h5 class="card-title mb-3 text-dark">Description</h5>
                    <p class="card-text mb-1">${annonceDet.description}</p>
                    <p style="color: #5c636a;font-size: 11px" class="mb-4">
                        Publié le ${annonceDet.dateCreated}
                        par
                         <span class="card-text text-capitalize text-primary">${annonceDet.author.username}</span>
                    </p>

                    <h5 class="card-title mb-3 text-dark">Prix</h5>
                    <div class="prixCardPart mb-4" style="font-size: 25px">
                        <g:formatNumber number="${annonceDet.price}" type="number" maxFractionDigits="0" /><span>,00€</span>
                    </div>


                </div>
            </div>
        </div>


    </div>

</div>
</body>
</html>
