<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />

    <asset:stylesheet src="application.css"/>
    <asset:stylesheet src="frontoffice.css"/>


    <g:layoutHead/>
</head>
<body>

%{--    <div class="navbar  navFront navbar-default navbar-static-top" role="navigation">--}%
%{--        <div class="container">--}%
%{--            <div class="navbar-header">--}%
%{--                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">--}%
%{--                    <span class="sr-only">Toggle navigation</span>--}%
%{--                    <span class="icon-bar"></span>--}%
%{--                    <span class="icon-bar"></span>--}%
%{--                    <span class="icon-bar"></span>--}%
%{--                </button>--}%
%{--                <a class="navbar-brand" href="/#">--}%
%{--		           <h2 class="logo"> LeCoinCoin </h2>--}%
%{--                </a>--}%
%{--            </div>--}%
%{--            <div class="navbar-collapse collapse" aria-expanded="false" style="height: 0.8px;">--}%
%{--                <ul class="nav navbar-nav navbar-right">--}%
%{--                    <form action="search/" method="get">--}%
%{--                        <input class="form-control" name="toSearch" type="text" placeholder="Search" aria-label="Chercher une annonce">--}%
%{--                    </form>--}%
%{--                </ul>--}%
%{--            </div>--}%
%{--        </div>--}%
%{--    </div>--}%
<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top" style="padding: 1em 1.5em">
    <div class="container-fluid">
        <a class="navbar-brand" href="#" style="font-family: 'Pacifico', cursive; font-size: 2vw !important;">leCoinCoin</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse d-flex" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item ">
                    <a class="nav-link active" aria-current="page" href="/">Accueil</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Annonce
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <g:pageProperty name="page.navann" />
                    </ul>
                </li>

            </ul>
            <form class="d-flex" action="/annonce/search/" method="get">
                <input class="form-control" name="toSearch" type="text" placeholder="Search" aria-label="Chercher une annonce">
            </form>
        </div>
    </div>
</nav>
    <g:layoutBody/>

%{--    <div class="footer" role="contentinfo"></div>--}%

    <div id="spinner" class="spinner" style="display:none;">
        <g:message code="spinner.alt" default="Loading&hellip;"/>
    </div>

    <asset:javascript src="application.js"/>

</body>
</html>
