package fr.itu.mbds.grails

import grails.gorm.transactions.Transactional
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import org.grails.web.json.JSONElement
import groovy.transform.CompileDynamic
import org.joda.time.DateTime

import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Transactional
class JSONConverterService {
    @CompileDynamic
    public Annonce annoncesFromJsonElement(JSONElement json) {
        Annonce annonce = new Annonce()
        if (json.id){
            annonce.id = json.id as Long
        }
        if (json.title){
            annonce.title = json.title as String
        }
        if (json.description){
            annonce.description = json.description as String
        }
        if (json.price){
            annonce.price = json.price as Float
        }
        if (json.status){
            annonce.status = json.status as Boolean
        }

        if (json.dateCreated){
            String a = json.dateCreated as String
            def pattern = "yyyy-MM-dd"
            def date = new SimpleDateFormat(pattern).parse(a)
            annonce.dateCreated =  date
        }
        if (json.lastUpdated){
            String a = json.lastUpdated as String
            def pattern = "yyyy-MM-dd"
            def date = new SimpleDateFormat(pattern).parse(a)

            annonce.lastUpdated = date
        }
        if (json.illustrations){
            annonce.illustrations = json.illustrations as List
        }

        if (json.author){
           def auth =  json.author.id as Long

            RestBuilder rest = new RestBuilder()
            String url = "http://localhost:8081/api/user/"+auth
            RestResponse restResponse = rest.get(url) {
                header("Accept","text/json")
            }

            annonce.author = UserFromJsonElement(restResponse.json)

        }

        annonce
    }

    public User UserFromJsonElement(JSONElement json) {
        User user = new User()
        println("id***"+json)
        if (json.id){
            user.id = json.id as Long
        }
        if (json.username){
            user.username = json.username as String
        }
        println("************"+user)
        user
    }


}
