package fr.itu.mbds.grails

import grails.gorm.services.Service
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse

@Service(Annonce)
interface AnnonceService {

    Annonce get(Serializable id)

    List<Annonce> list(Map args)

    Long count()

    void delete(Serializable id)

    Annonce save(Annonce annonce)
}