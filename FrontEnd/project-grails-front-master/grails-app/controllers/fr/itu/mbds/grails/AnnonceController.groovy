package fr.itu.mbds.grails

import grails.plugin.springsecurity.annotation.Secured
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import grails.validation.ValidationException

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import static org.springframework.http.HttpStatus.*

@Secured(['permitAll'])
class AnnonceController {

    AnnonceService annonceService
    IllustrationService illustrationService
    JSONConverterService jsonService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
//        params.max = Math.min(max ?: 10, 100)

        List<Annonce> annonceList = new ArrayList<Annonce>()
        RestBuilder rest = new RestBuilder()
        String url = "http://localhost:8081/api/annonces"
        RestResponse restResponse = rest.get(url) {
            header("Accept","text/json")
        }
        def listeAnnonce = []
        restResponse.json.each {
            def temp =  new JSONConverterService().annoncesFromJsonElement(it)
            listeAnnonce.add(temp)
        }
        render (view: 'index', model:[annonceDet:listeAnnonce]);
    }


    def search(String toSearch) {
//        params.max = Math.min(max ?: 10, 100)

        List<Annonce> annonceList = new ArrayList<Annonce>()
        RestBuilder rest = new RestBuilder()
        String url = "http://localhost:8081/api/annonces"
        RestResponse restResponse = rest.get(url) {
            header("Accept","text/json")
        }
        def listeAnnonce = []

        restResponse.json.each {
            def temp =  new JSONConverterService().annoncesFromJsonElement(it)
            if(temp.title.toUpperCase().contains(toSearch.toUpperCase()) || temp.description.toUpperCase().contains(toSearch.toUpperCase())){
                listeAnnonce.add(temp)
            }
        }


        render (view: 'search', model:[annonceDet:listeAnnonce]);
    }



    def show(Long id) {

        RestBuilder rest = new RestBuilder()
        String url = "http://localhost:8081/api/annonce/"+id
        RestResponse restResponse = rest.get(url) {
            header("Accept","text/json")
        }
        def annonceDet =   new JSONConverterService().annoncesFromJsonElement( restResponse.json)
        render (view: 'show', model:[annonceDet:annonceDet]);
    }

    def create() {
        def usersAuthors = User.all
        render (view: 'create', model:[userList:usersAuthors]);
        respond new Annonce(params)
    }

    def save(Annonce annonce) {
        if (annonce == null) {
            notFound()
            return
        }
        String imageUploadPath= grailsApplication.config.getProperty('illustrations.path')
        request.getMultiFileMap().illustration.each {

            if(it && !it.empty){
                it.transferTo(new File("${imageUploadPath}${it.originalFilename}"))
                flash.message="your.sucessful.file.upload.message"
            }
            else{
                flash.message="your.unsucessful.file.upload.message"
            }
            annonce.addToIllustrations(new Illustration(filename: "${it.originalFilename}"))
        }

        try {
            annonceService.save(annonce)
        } catch (ValidationException e) {
            respond annonce.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'annonce.label', default: 'Annonce'), annonce.id])
                redirect annonce
            }
            '*' { respond annonce, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond annonceService.get(id)
    }

    def update(Annonce annonce) {
        if (annonce == null) {
            notFound()
            return
        }

        try {
            annonceService.save(annonce)
        } catch (ValidationException e) {
            respond annonce.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'annonce.label', default: 'Annonce'), annonce.id])
                redirect annonce
            }
            '*'{ respond annonce, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        annonceService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'annonce.label', default: 'Annonce'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def saveImage(){
        def annonce = annonceService.get(params.idA)
        String imageUploadPath= grailsApplication.config.getProperty('illustrations.path')
        request.getMultiFileMap().illustration.each {

            if(it && !it.empty){
                it.transferTo(new File("${imageUploadPath}${it.originalFilename}"))
                flash.message="your.sucessful.file.upload.message"
            }
            else{
                flash.message="your.unsucessful.file.upload.message"
            }
            annonce.addToIllustrations(new Illustration(filename: "${it.originalFilename}"))
        }
        this.update(annonce)
    }
    def uploadImage(){
        def file=request.getFile('image')
        String imageUploadPath=grailsApplication.config.imageUpload.path
        try{
            if(file && !file.empty){
                file.transferTo(new File("${imageUploadPath}/${file.name}"))
                flash.message="your.sucessful.file.upload.message"
            }
            else{
                flash.message="your.unsucessful.file.upload.message"
            }
        }
        catch(Exception e){
            log.error("Your exception message goes here",e)
        }

    }


    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'annonce.label', default: 'Annonce'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
